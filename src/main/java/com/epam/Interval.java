
package com.epam;

import java.util.Scanner;
/**
 * Class Interval.
 * Work with interval
 */
class Interval {
    /**array - is array which contain an interval.*/
    static int[] array;
    /**minValue saves start value of interval.*/
    private static int minValue;
    /**maxValue saves last value of interval.*/
    private static int maxValue;
    /**sizeArray saves size of array.*/
    private static int sizeArray;
/**
Text of program
 */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the intervals limits:");
        System.out.print("From: ");
        minValue = in.nextInt();
        System.out.print("To: ");
        maxValue = in.nextInt();

        System.out.println("Entered interval: ["
                + minValue + ";" + maxValue + "]");

        sizeArray = maxValue - minValue + 1;

        array = new int[sizeArray];

        for (int i = 0; i < sizeArray; i++) {
            array[i] = minValue;
            minValue++;
        }
         /**
          * PrintOddNumber method prints odd numbers from start to
          * the end of interval and even from end to start.
          */
            { System.out.println("The Odd Numbers are:");
                for (int i = 0; i <= sizeArray; i++) {
                    if (i % 2 == 1) {
                        System.out.print(i + " ");
                    }
                }
                System.out.println(" ");
            }
            System.out.println("The Even Numbers are:");
            for (int i = sizeArray - 1; i >= 0; i--) {
                if (i % 2 != 0) {
                    System.out.print(i + " ");
                }
            }
        }
    }

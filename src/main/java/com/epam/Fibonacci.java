package com.epam;
import java.util.Scanner;
/**
 * Class Fibonacci.
 * Work with the Fibonacci numbers
 */
class Fibonacci {
    /**array is array of set Fibonacci numbers.*/
    private static int[] array;
    /** sizeOfSet is size of set Fibonacci numbers(N).*/
    private static int sizeOfSet;
    /** maxOdd is the biggest odd
     *  number of Fibonacci numbers(F1).*/
    private static int maxOdd;
    /** maxEven is the biggest even
     * number of Fibonacci numbers(F2).*/
    private static int maxEven;
    /**
     * Program build:F1 will
     * be the biggest odd number and F2 – the biggest even
     * number, user can enter the size of set (N).
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        /** stopF1 = false, when F1 write*/
        boolean stopF1 = true;
        /** stopF2 = false, when F2 write*/
        boolean stopF2 = true;
        System.out.print("Enter the size of set: ");
        sizeOfSet = in.nextInt();
        array = new int[sizeOfSet];
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < sizeOfSet; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }
        System.out.println("Formed Fibonacci sequence: ");
        for (int i = 0; i < sizeOfSet; i++) {
            System.out.print(array[i] + " ");
        }
        for (int i = sizeOfSet - 1; i >= 0; i--) {
            if ((array[i] % 2 == 1) && stopF1) {
                maxOdd = array[i];
                stopF1 = false;
            } else if ((array[i] % 2 == 0) && stopF2) {
                maxEven = array[i];
                stopF2 = false;
            }
        }
        System.out.println("\nBiggest odd number "
                + "from Fibonacci sequence: " + maxOdd);
        System.out.println("Biggest even number "
                + "from Fibonacci sequence: " + maxEven);
        {
            /**
             * Percentage method prints percentage of
             * odd and even Fibonacci numbers.
             */
            int percentagemax = 100;
            int percentageOdd;
            int percentageEven;
            int sizeOfOdd = 0;
            int sizeOfEven = 0;

            for (int i = 0; i < sizeOfSet; i++) {
                if (array[i] % 2 == 1) {
                    sizeOfOdd++;
                } else {
                    sizeOfEven++;
                }
            }
            percentageOdd = sizeOfOdd * percentagemax / sizeOfSet;
            percentageEven = sizeOfEven * percentagemax / sizeOfSet;

            System.out.println("Odd numbers: " + percentageOdd + "%");
            System.out.println("Even numbers: " + percentageEven + "%");
        }
    }
}